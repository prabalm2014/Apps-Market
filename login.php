<?php
	session_start();
	$username =$password ="";
	$Err="";
	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$ok=1;
		if(empty($_POST["username"]) && empty($_POST["password"]))
		{
			$Err = "Server Error";$ok=0;
		}
		else
		{
			$username = test_input($_POST["username"]);
			$_SESSION["username"] = $username;
			$password = test_input($_POST["password"]);
			$_SESSION["password"] = $password;
		}
		if(!empty($_POST["submit"]))
		{
			$Username = $_SESSION['username'];
			$Passqord = $_SESSION["password"];
			
			$servername = "localhost";
			$usnam = "root";
			$passw = "";
			$dbname = "appsstore";
			
			$conn = mysqli_connect($servername, $usnam, $passw, $dbname);
			if (!$conn) 
			{
				die("Connection failed: " . mysqli_connect_error());
			}
			if (!empty($_POST["username"]) && !empty($_POST["password"])) 	
			{
				$check = mysqli_query($conn,"select username,password from user where username='$Username' and password='$Passqord'");
				$checkrows=mysqli_num_rows($check);
				if($checkrows>0)
				{
					header('Location: afterlogin.php');
					exit();
				}
				else
				{
					$Err= "&#9932 Password and Username Not Matched!";$ok=0;
					$password ="";
				}
			}
		}
	}
	function test_input($data) 
	{
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Login</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="style.css" type="text/css"/>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>
		<script type="text/javascript">
				$(function(){
					$("#login").validate({
					errorElement: "div",
					errorPlacement: function(error, element) {
					error.appendTo('div#bos');
					$(".bo").show();
					},
						rules:{
							username:{
								required: true,
								email: true,
							},
							password:{
								required: true,
							},
						},
						messages:{
							username:{
								required: "&#9932 Please Enter Your Username!",
								email:"&#9932 Please Enter a Valid Email or Username!",
							},
							password: {
								required: "&#9932 Please Enter Your Password!",
							},
						},
						submitHandler: function(form) {
						form.submit();
						}
					});
				});
		</script>
	</head>
	<body lang="en-US">
		<img class="img1" src="icon/room.jpg" alt="Apps Market">
		<div id = "main">
			<header class="hed">
			<div class="logo">
				<img class="img2" src="icon/logo.png" alt="Apps Market">
				<p class="app"><span class="s1">A</span>pps <span class="s2"><span class="s3">M</span>arket</span></p>
			</div>
			<ul>
				<li><a href="about.php">About</a></li>
				<li><a href="home.php">Home</a></li>
			</ul>
			</header>
			
			<div id="opa">
				<form name="login" action="#" method="post" id="login">
					<br/>
					<input class="unt" type="email" name="username" id="username" required placeholder=" Username,Email" value="<?php echo $username?>"></br>
					<input class="pat" type="password" name="password" id="password" placeholder=" Password" value="<?php echo $password?>"><span style="color:red;"><?php echo $Err ?></span><br/>
					<div class="bo" id="bos"></div>
					<input class="sub" name="submit" type="submit" value="Log in" id="submit"/><br/><br/>
					<a href="signup.php">No account? Signup Here.</a><br/><br/>
				</form>
			</div>
			<div id="welcome">
				<h1 class="wel">Welcome to Apps Market.</h1>
				<p class="wrt">Sign in to upload your application in Apps Market.</br>See new application and download it.</p>
			</div>
		</div>
	</body>
</html>
