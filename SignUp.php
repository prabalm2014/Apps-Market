<?php
	session_start();
	$fname = $username =$pass1= $pass2= $gender="";
	$Err="";
	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$ok=1;
		if(empty($_POST["fname"]) && empty($_POST["username"]) && empty($_POST["pass1"]) && empty($_POST["pass2"]) && empty($_POST["gender"]))
		{
			$Err = "Server Error";$ok=0;
		}
		else
		{
			$fname = test_input($_POST["fname"]);
			$_SESSION["fname"] = $fname;
			$username = test_input($_POST["username"]);
			$_SESSION["username"] = $username;
			$pass1 = test_input($_POST["pass1"]);
			$_SESSION["pass1"] = $pass1;
			$pass2 = test_input($_POST["pass2"]);
			$_SESSION["pass2"] = $pass2;
			$gender = test_input($_POST["gender"]);
			$_SESSION["gender"] = $gender;
		}
		if(!empty($_POST["submit"]))
		{
			$Fname = $_SESSION['fname'];
			$Username = $_SESSION['username'];
			$Pass2 = $_SESSION["pass2"];
			$Gender = $_SESSION["gender"];
			
			$servername = "localhost";
			$usnam = "root";
			$passw = "";
			$dbname = "appsstore";
			
			$conn = mysqli_connect($servername, $usnam, $passw, $dbname);
			if (!$conn) 
			{
				die("Connection failed: " . mysqli_connect_error());
			}
			if (!empty($_POST["fname"]) && !empty($_POST["username"]) && !empty($_POST["pass1"]) && !empty($_POST["pass2"]) && !empty($_POST["gender"])) 	
			{
				$sql = "INSERT INTO user (fullname,  username, password, gender)
				VALUES ('$fname', '$Username', '$pass2', '$gender')";

				if (mysqli_query($conn, $sql) && !empty($_POST["fname"]) && !empty($_POST["username"]) && !empty($_POST["pass1"]) && !empty($_POST["pass2"]) && !empty($_POST["gender"])) 
				{
					$fname = $username =$pass1= $pass2=$gender="";
					header('Location: profile.php');
					exit();
				} 
				$sql1 = "select username from user where username = '".$Username."' ";
				$result =  mysqli_query($conn, $sql1);
				if($result->num_rows > 0)
				{
					$row = $result->fetch_assoc();
					$conn->close();
					$Err = "&#9932 Username Already Exist Try Another Email or Username!";$ok=0;
				}
				
				else 
				{
					echo "Error: " . $sql . "<br>" . $conn->error;
				}
			}
		}
	}
	function fup()
	{
		
	}
	function test_input($data) 
	{
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Sign Up</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="style.css" type="text/css"/>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>
		<script type="text/javascript">
		$(function(){
			$("#signup").validate({
			errorElement: "div",
			errorPlacement: function(error, element) {
			error.appendTo('div#bos');
			$(".bo").show();
			},
				rules:{
					fname:{
						required: true,
					},
					username:{
						required: true,
						email: true,
					},
					pass1:{
						required: true,
						minlength:6,
						maxlength:24
					},
					pass2:{
						required: true,
						minlength:6,
						maxlength:24,
						equalTo: "#pass1"
					},
					gender:{
						required: true,
					},
				},
				messages:{
					fname:{
						required: "&#9932 Please Enter Your Fullname!",
					},
					username:{
						required: "&#9932 Please Enter Your Username!",
						email:"&#9932 Please Enter a Valid Email or Username!",
					},
					pass1: {
						required: "&#9932 Please Enter Your Password!",
						minlength: "&#9932 Password Should Not be Less Than 6 Characters!",
						maxlength: "&#9932 Password Should Not be More Than 24 Characters!",
					},
					pass2: {
						required: "&#9932 Please Retype Your Password!",
						minlength: "&#9932 Password Should Not be Less Than 6 Characters!",
						maxlength: "&#9932 Password Should Not be More Than 24 Characters!",
						equalTo: "&#9932 Please Enter The Same Password!",
					},
					gender:{
						required: "&#9932 Please Select Your Gender!",
					},
				},
				submitHandler: function(form) {
				form.submit();
				}
			});
		});
	</script>
	</head>
	<body lang="en-US">
		<img class="img1" src="icon/signup.jpg" alt="Apps Market">
		<div id = "main">
			<header class="hed">
			<div class="logo">
				<img class="img2" src="icon/logo.png" alt="Apps Market">
				<p class="app"><span class="s1">A</span>pps <span class="s2"><span class="s3">M</span>arket</span></p>
			</div>
			<ul>
				<li><a href="about.php">About</a></li>
				<li><a href="home.php">Home</a></li>
			</ul>
			</header>
			
			<div id="sup">
				<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="post" name="signup" id="signup">
					<input name="fname" id="fname" class="sunt" type="text" required placeholder=" Fullname" value="<?php echo $fname?>"/></br>
					<input name="username" id="username" class="sunt" type="email" required placeholder=" Username,Email" value="<?php echo $username?>"/></br>
					<input name="pass1" id="pass1" class="sunt" type="password" required placeholder=" Password" value="<?php echo $pass1?>"/></br>
					<input name="pass2" id="pass2" class="sunt" type="password" required placeholder=" Retype-Password" value="<?php echo $pass2?>"/></br><br/>
					<label class="unl" for='gender'>Gender</label></br>
					<input name="gender" id="gen" type="radio" value="Male"/><label class="unl">Male</label>
					<input name="gender" id="gen" type="radio" value="Female"/><label class="unl">Female</label></br>
					<div class="bo" id="bos"><?php echo $Err ?></div>
					<input class="sub" name="submit" type="submit" value="Sign Up"/>
				</form>
			</div>
			<div id="welcome2">
				<h1 class="wel">Welcome to Apps Market.</h1>
				<p class="wrt">Sign Up to stay tuned with Apps Market.</br>Upload your apps and download new apps.</p>
			</div>
		</div>
	</body>
</html>