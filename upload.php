<?php
	session_start();
	if(!isset($_SESSION["username"]))
	{
		header('Location : login.php');
	}
	$name = $platform =$category= $company= $link= $description= $picture="";
	$Err="";
	if($_SERVER["REQUEST_METHOD"] == "POST")
	{
		$ok=1;
		if(empty($_POST["name"]) && empty($_POST["platform"]) && empty($_POST["category"]) && empty($_POST["company"]) && empty($_POST["link"]) && empty($_POST["description"]) && empty($_POST["picture"]))
		{
			$Err = "Server Error";$ok=0;
		}
		else
		{
			$name = test_input($_POST["name"]);
			$_SESSION["name"] = $name;
			
			$platform = test_input($_POST["platform"]);
			$_SESSION["platform"] = $platform;
			
			$category = test_input($_POST["category"]);
			$_SESSION["category"] = $category;
			
			$company = test_input($_POST["company"]);
			$_SESSION["company"] = $company;
			
			$link = test_input($_POST["link"]);
			$_SESSION["link"] = $link;
			
			$description = test_input($_POST["description"]);
			$_SESSION["description"] = $description;
			
			$_SESSION["picture"] = $picture;
		}
		//image
		$uploadOk = 1;
		$fileExtension = pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION);
		$Err =  $_FILES['picture']['name'];
		if($_FILES['picture']['name'])
		{
			if(!$_FILES['picture']['error'])
			{
				$target = "uploads/"; 
				$target = $target . basename($name.'.'.$fileExtension);
				$picture = $target;
			}
			else
				$uploadOk = 0;
				$Err = "didn't get the file name";
		}
		
		if ($_FILES['picture']['size'] > 1024*1024) {
			$Err = "Sorry, your file is too large.";
			$uploadOk = 0;
		}
		
		if($fileExtension != "jpg" && $fileExtension != "png" && $fileExtension != "jpeg" && $fileExtension != "gif" && $fileExtension != "bmp")
		{
			$Err = "only jpg, png, jpeg & gif files are allowed";
			$uploadOk = 0;
		}
		
		else
		{
			if(move_uploaded_file($_FILES['picture']['tmp_name'], $target)) 
			{
				$Err = "The file ". basename( $_FILES['picture']['name']). " has been uploaded <br>";
			} 
			else
			{
				$Err = "Sorry, there was a problem uploading your file.";
			}
		} 
			
		
		//database
		if($uploadOk == 1 && $ok == 1)
		{
			$Username = $_SESSION['username'];
			
			$servername = "localhost";
			$usnam = "root";
			$passw = "";
			$dbname = "appsstore";
			
			$conn = mysqli_connect($servername, $usnam, $passw, $dbname);
			if (!$conn) 
			{
				die("Connection failed: " . mysqli_connect_error());
			}
		
			$sql = "INSERT INTO apps (username,  apps_name, platform, categories, company, link, description, picture,date, time)
			VALUES ('$Username', '$name', '$platform', '$category', '$company', '$link', '$description', '$picture',CURDATE(),CURTIME())";

			if (mysqli_query($conn, $sql)) 
			{
				$name = $platform =$category= $company= $link= $description="";
				mysqli_close($conn);
				header('Location: afterlogin.php');
				exit();
			} 
			else 
			{
				echo "Error: " . $sql . "<br>" . $conn->error;
			}
		}
		else{
			$Err = "database Error";
		}
	}
	
	function test_input($data) 
	{
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Upload</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="style.css" type="text/css"/>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>
		<script type="text/javascript">
				$(function(){
					$("#upload").validate({
						rules:{
							name:{
								required: true,
								maxlength:50
							},
							platform:{
								required: true
							},
							category:{
								required: true
							},
							company:{
								required: true
							},
							link:{
								required: true,
								url:true
							},
							description:{
								required: true
							},
							picture:{
								required: true
							},
						},
						messages:{
							name:{
								required: "&#9932 Please enter apps name!",
								maxlength:"&#9932 Apps name should not be more than 50 characters!",
							},
							platform:{
								required: "&#9932 Please enter apps platform!",
							},
							category:{
								required: "&#9932 Please enter apps category!",
							},
							company:{
								required: "&#9932 Please enter your company or instituate name!",
							},
							link:{
								required: "&#9932 Please insert apps link!",
								url: "&#9932 Please insert valid url!",
							},
							description:{
								required: "&#9932 Tell about the app!",
							},
							picture:{
								required: "&#9932 Upload a app picture!",
							},
						},
						submitHandler: function(form) {
						form.submit();
						}
					});
				});
		</script>
	</head>
	
	<body class="upbd" lang="en-US">
		<div id = "main">
			<header class="hed1">
			<div class="logo">
				<img class="img2" src="icon/logo.png" alt="Apps Market">
				<p class="app1">Apps Market</p>
			</div>
			</header>
			<div id="upld">
				<form name="upload" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="post" id="upload" enctype="multipart/form-data">
					<br/>
					<input class="nam" id="name" type="text" name="name" required placeholder=" Apps Name" value="<?php echo $name?>"></br>
					<input class="nam" id="platform" list="Platform" name="platform" required placeholder=" Platform" value="<?php echo $platform?>"></br>
					<datalist id="Platform">
						<option value="Android">
						<option value="Windows Desktop">
						<option value="Windows Mobile">
						<option value="I phone">
				    </datalist>
					<input class="nam" id="category" list="Categories" name="category" required placeholder=" Categories" value="<?php echo $category?>"></br>
					<datalist id="Categories">
						<option value="Games">
						<option value="Entertainment">
						<option value="Media & video">
						<option value="Music & Audio">
						<option value="Education">
						<option value="Medical">
						<option value="History">
						<option value="Live Wallpaper">
						<option value="Personalization">
						<option value="Photography">
						<option value="Productivity">
						<option value="Sports">
						<option value="Weather">
						<option value="Transportation">
						<option value="Travel">
						<option value="News">
				    </datalist>
					<input class="nam" type="text" name="company" id="company" placeholder=" Company, Versity or Personal Project" value="<?php echo $company?>"></br>
					<input class="nam" type="url" name="link" id="link" required placeholder=" Apps Link" pattern="https?://.+" value="<?php echo $link?>"></br>
					<textarea id="description" name="description" rows="4" cols="39" required placeholder="Apps Description" value="<?php echo $description?>"></textarea></br>
					<label style="color:#001a00;padding-top:8px;">Apps Picture</label>
					<input type="file" name="picture" id="picture" required><br/><br/>
					<div class="bo" id="bos"></div>
					<span><?php echo $Err ;?></span>
					<a href="afterlogin.php"><input type="button" class="sub1" name="back" value="Back"></a><input class="sub1" type="submit" name="submit" value="Upload" id="submit"/><br/><br/>
				</form>
			</div>
			<div id="intro">
				<h1 class="up">Upload Your Apps</h1>
				<h3 class="sh">Share your creativity with all. Create some unique <br/>application, upload apps link into apps market.</h3><br/>
				<p class="op">&#9758  Upload apps in a cloud drive.</p>
				<p class="op">&#9758  Copy the apps link from cloud drive.</p>
				<p class="op">&#9758  Upload apps link into apps market.</p>
			</div>
		</div>
	</body>
</html>
