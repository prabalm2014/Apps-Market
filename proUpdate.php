<?php
	session_start();
	if(!isset($_SESSION["username"]))
	{
		header('Location: home.php');
	}
	$id= $_GET["id"];
	$userN = $_SESSION["username"];
	
	if($_SERVER["REQUEST_METHOD"] != "POST")
	{
		$server = "localhost";
		$db = "appsstore";
		
		$conn = mysqli_connect($server, "root", "", $db);
		
		$query = "select * from user where username = '$userN'";
		$result = mysqli_query($conn, $query);
		
		if (!$conn) {
			die("Connection failed: " . mysqli_connect_error());
		}
		
		if(!$result)
		{
			echo $conn->error;
		}
		else
		{
			$data = mysqli_fetch_assoc($result);
			$fname = $data["fullname"];
			$pass1 = $data["password"];
			$pass2 = $data["password"];
		}
	}
	else
	{	
		$fname = test_input($_POST["fname"]);
		$pass1 = test_input($_POST["pass1"]);
		$pass2 = test_input($_POST["pass2"]);
		
		$server = "localhost";
		$db = "appsstore";
		$conn = mysqli_connect($server, "root", "", $db);
		
		$query = "UPDATE user set fullname = '$fname', password = '$pass2' where username = '$userN';";
		
		
		if (!$conn) {
			die("Connection failed: " . mysqli_connect_error());
		}
		
		if(mysqli_query($conn, $query))
		{
			mysqli_close($conn);
			header('Location: profile.php');
		}
		else
			echo $conn->error;
	}
	
	function test_input($data)
	{
	   $data = trim($data);
	   $data = stripslashes($data);
	   $data = htmlspecialchars($data);
	   return $data;
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Update Profile</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="style.css" type="text/css"/>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>
		<script type="text/javascript">
		$(function(){
			$("#proupdate").validate({
				rules:{
					fname:{
						required: true,
					},
					pass1:{
						required: true,
						minlength:6,
						maxlength:24
					},
					pass2:{
						required: true,
						minlength:6,
						maxlength:24,
						equalTo: "#pass1"
					},
				},
				messages:{
					fname:{
						required: "&#9932 Please Enter Your Fullname!",
					},
					pass1: {
						required: "&#9932 Please Enter Your Password!",
						minlength: "&#9932 Password Should Not be Less Than 6 Characters!",
						maxlength: "&#9932 Password Should Not be More Than 24 Characters!",
					},
					pass2: {
						required: "&#9932 Please Retype Your Password!",
						minlength: "&#9932 Password Should Not be Less Than 6 Characters!",
						maxlength: "&#9932 Password Should Not be More Than 24 Characters!",
						equalTo: "&#9932 Please Enter The Same Password!",
					},
				},
				submitHandler: function(form) {
				form.submit();
				}
			});
		});
		</script>
	</head>
	<body class="upbd" lang="en-US">
		<div id = "main">
			<header class="hed1">
			<div class="logo">
				<img class="img2" src="icon/logo.png" alt="Apps Market">
				<p class="app1">Apps Market</p>
			</div>
			</header>
			<div id="updt">
				<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="post" name="proupdate" id="proupdate">
					<h1 align="center">Update Profile</h1>
					<input name="fname" id="fname" class="nam1" type="text" required placeholder=" Fullname" value="<?php echo $fname?>"/></br>
					<input name="pass1" id="pass1" class="nam1" type="password" required placeholder=" Password" value="<?php echo $pass1?>"/></br>
					<input name="pass2" id="pass2" class="nam1" type="password" required placeholder=" Retype-Password" value="<?php echo $pass2?>"/></br><br/>
					<a href="afterlogin.php"><input type="button" class="sub1" name="back" value="Back"></a><input class="sub1" type="submit" name="submit" value="Update" id="submit"/><br/><br/>
				</form>
			</div>
		</div>
	</body>
</html>
