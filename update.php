<?php
	session_start();
	if(!isset($_SESSION["username"]))
	{
		header('Location: home.php');
	}
	$id= $_GET["id"];
	$userN = $_SESSION["username"];
	
	if($_SERVER["REQUEST_METHOD"] != "POST")
	{
		$server = "localhost";
		$db = "appsstore";
		
		$conn = mysqli_connect($server, "root", "", $db);
		
		$query = "select * from apps where a_id = '$id'";
		$result = mysqli_query($conn, $query);
		
		if (!$conn) {
			die("Connection failed: " . mysqli_connect_error());
		}
		
		if(!$result)
		{
			echo $conn->error;
		}
		else
		{
			$data = mysqli_fetch_assoc($result);
			$aname = $data["apps_name"];
			$platf = $data["platform"];
			$catg = $data["categories"];
			$comp = $data["company"];
			$lnk = $data["link"];
			$descr = $data["description"];
		}
	}
	else
	{	
	
		$fileExtension = pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION);
		$Err =  $_FILES['picture']['name'];
		if($_FILES['picture']['name'])
		{
			if(!$_FILES['picture']['error'])
			{
				$target = "uploads/"; 
				$target = $target . basename($name.'.'.$fileExtension);
				$picture = $target;
			}
			else
				$uploadOk = 0;
				$Err = "didn't get the file name";
		}
		
		if ($_FILES['picture']['size'] > 1024*1024) {
			$Err = "Sorry, your file is too large.";
			$uploadOk = 0;
		}
		
		if($fileExtension != "jpg" && $fileExtension != "png" && $fileExtension != "jpeg" && $fileExtension != "gif" && $fileExtension != "bmp")
		{
			$Err = "only jpg, png, jpeg & gif files are allowed";
			$uploadOk = 0;
		}
		
		else
		{
			if(move_uploaded_file($_FILES['picture']['tmp_name'], $target)) 
			{
				$Err = "The file ". basename( $_FILES['picture']['name']). " has been uploaded <br>";
				header('Location: editProfile.php');
			//	echo "<pre>".var_dump($_FILES)."</pre>";
			} 
			else
			{
				$Err = "Sorry, there was a problem uploading your file.";
			}
		} 
	
		$aname = test_input($_POST["name"]);
		$platf = test_input($_POST["platform"]);
		$catg = test_input($_POST["category"]);
		$comp = test_input($_POST["company"]);
		$lnk = test_input($_POST["link"]);
		$descr = test_input($_POST["description"]);
		
		$server = "localhost";
		$db = "appsstore";
		$conn = mysqli_connect($server, "root", "", $db);
		
		$query = "UPDATE apps set apps_name = '$aname', platform = '$platf',
			categories = '$catg', company = '$comp', link = '$lnk',description = '$descr', picture = '$picture'
			where a_id = '$id';";
		
		
		if (!$conn) {
			die("Connection failed: " . mysqli_connect_error());
		}
		
		if(mysqli_query($conn, $query))
		{
			mysqli_close($conn);
			header('Location: afterlogin.php');
		}
		else
			echo $conn->error;
	}
	
	function test_input($data)
	{
	   $data = trim($data);
	   $data = stripslashes($data);
	   $data = htmlspecialchars($data);
	   return $data;
	}
	
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Update Apps</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="style.css" type="text/css"/>
	</head>
	
	<body class="upbd" lang="en-US">
		<div id = "main">
			<header class="hed1">
			<div class="logo">
				<img class="img2" src="icon/logo.png" alt="Apps Market">
				<p class="app1">Apps Market</p>
			</div>
			</header>
			<div id="updt">
				<form ction = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="POST"  enctype="multipart/form-data" id="upload">
					<h1 align="center">Update Apps</h1>
					<input class="nam1" id="name" type="text" name="name" placeholder=" Apps Name"  value="<?php echo $aname;?>"></br>
					<input class="nam1" id="platform" list="Platform" name="platform" placeholder=" Platform" value="<?php echo $platf;?>"></br>
					<datalist id="Platform">
						<option value="Android">
						<option value="Windows Desktop">
						<option value="Windows Mobile">
						<option value="I phone">
				    </datalist>
					<input class="nam1" id="category" list="Categories" name="category" placeholder=" Categories" value="<?php echo $catg;?>"></br>
					<datalist id="Categories">
						<option value="Games">
						<option value="Entertainment">
						<option value="Media & video">
						<option value="Music & Audio">
						<option value="Education">
						<option value="Medical">
						<option value="History">
						<option value="Live Wallpaper">
						<option value="Personalization">
						<option value="Photography">
						<option value="Productivity">
						<option value="Sports">
						<option value="Weather">
						<option value="Transportation">
						<option value="Travel">
						<option value="News">
				    </datalist>
					<input class="nam1" type="text" name="company" id="company" placeholder=" Company, Versity or Personal Project" value="<?php echo $comp;?>"></br>
					<input class="nam1" type="url" name="link" id="link" placeholder=" Apps Link" pattern="https?://.+" value="<?php echo $lnk;?>"></br>
					<textarea class="description1" id="descrip" name="description" rows="4" cols="39" placeholder="Apps Description" value="<?php echo $descr;?>"></textarea></br>
					<label style="color:#001a00;padding-top:8px;">Apps Picture</label>
					<input type="file" name="picture" id="picture" value="<?php echo $photo;?>"><br/><br/>
					<span><?php echo $Err ;?></span><br/>
					<a href="afterlogin.php"><input type="button" class="sub1" name="back" value="Back"></a><input class="sub1" type="submit" name="submit" value="Update" id="submit"/><br/><br/>
				</form>
			</div>
		</div>
	</body>
</html>
